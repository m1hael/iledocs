package iledocs.rest;

import org.junit.Assert;
import org.junit.Test;

public class VersionTest {

	@Test
	public void test_parse() {
		Assert.assertEquals("1.0.0.0", new Version("1.0.0").toString());
		Assert.assertEquals("1.0.0.0", new Version("1.0").toString());
		Assert.assertEquals("1.0.0.0", new Version("1").toString());
		Assert.assertEquals("0.0.0.0", new Version("").toString());
		Assert.assertEquals("1.0.2.0", new Version("1.0.2").toString());
		Assert.assertEquals("1.0.2.0-RC1", new Version("1.0.2-RC1").toString());
		Assert.assertEquals("1.4.0.0", new Version("1.4").toString());
	}
	
	@Test
	public void test_compare() {
		Assert.assertEquals(0, new Version("1.0").compareTo(new Version("1.0.0")));
		Assert.assertTrue(new Version("1.1.0").compareTo(new Version("2")) < 0);
		Assert.assertTrue(new Version("2").compareTo(new Version("2-RC1")) > 0);
		Assert.assertTrue(new Version("2.0.0-RC1").compareTo(new Version("2-RC2")) < 0);
	}
}
