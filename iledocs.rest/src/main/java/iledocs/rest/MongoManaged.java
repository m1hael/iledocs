package iledocs.rest;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoDatabase;

import io.dropwizard.lifecycle.Managed;

/**
 * Wrapper component for the Mongo DB Access component.
 * 
 * @author Mihael Schmidt
 */
public class MongoManaged implements Managed {
    
    private final MongoClient client;
    private MongoDatabase db;

    public MongoManaged(MongoClient client) {
        this.client = client;
        db = client.getDatabase("iledocs");
    }
    
    @Override
    public void start() throws Exception {
    }

    @Override
    public void stop() throws Exception {
        client.close();
    }
    
    public MongoDatabase getDatabase() {
        return db;
    }
}
