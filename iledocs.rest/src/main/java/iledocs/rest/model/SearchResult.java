package iledocs.rest.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SearchResult {

	private String searchTerm;
	private int hits;
	private List<SearchResultEntry> entries = new ArrayList<>();

	public SearchResult() {

	}

	public int getHits() {
		return hits;
	}

	public void setHits(int hits) {
		this.hits = hits;
	}

	public List<SearchResultEntry> getEntries() {
		return entries;
	}

	public void setEntries(List<SearchResultEntry> entries) {
		this.entries = entries;
	}

	public void addEntry(SearchResultEntry entry) {
		this.entries.add(entry);
	}
	
	public void addEntries(Collection<SearchResultEntry> entries) {
		this.entries.addAll(entries);
	}

	public String getSearchTerm() {
		return searchTerm;
	}

	public void setSearchTerm(String searchTerm) {
		this.searchTerm = searchTerm;
	}

}
