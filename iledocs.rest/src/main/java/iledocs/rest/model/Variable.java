package iledocs.rest.model;

import java.util.List;

public class Variable {

	public enum Type {
		Integer, Char, Varchar, Binary, PackedDecimal, ZonedDecimal, Float, Pointer, UnsignedInteger, Boolean, Date, Timestamp, Time, DataStructure, Graph, Object, UCS2
	}

	private String name;
	private String description;
	private Type type;
	private Integer length;
	private Integer decimalPositions;
	private List<String> keywords;
	private String like;
	private boolean exported;
	private boolean imported;
	private Integer arraySize;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	public Integer getDecimalPositions() {
		return decimalPositions;
	}

	public void setDecimalPositions(Integer decimalPositions) {
		this.decimalPositions = decimalPositions;
	}

	public List<String> getKeywords() {
		return keywords;
	}

	public void setKeywords(List<String> keywords) {
		this.keywords = keywords;
	}

	public String getLike() {
		return like;
	}

	public void setLike(String like) {
		this.like = like;
	}

	public boolean isExported() {
		return exported;
	}

	public void setExported(boolean exported) {
		this.exported = exported;
	}

	public boolean isImported() {
		return imported;
	}

	public void setImported(boolean imported) {
		this.imported = imported;
	}

	public Integer getArraySize() {
		return arraySize;
	}

	public void setArraySize(Integer arraySize) {
		this.arraySize = arraySize;
	}
	
	@Override
	public String toString() {
		return name;
	}

}
