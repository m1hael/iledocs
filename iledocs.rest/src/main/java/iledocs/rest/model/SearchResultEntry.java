package iledocs.rest.model;

public class SearchResultEntry {

	public enum Type {
		Module, Procedure, Variable, Constant, File
	}

	private String project;
	private String source;
	private String value;
	private Type type;

	public SearchResultEntry() {

	}

	public SearchResultEntry(String project, String source, String value, Type type) {
		this.project = project;
		this.source = source;
		this.value = value;
		this.type = type;
	}

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
