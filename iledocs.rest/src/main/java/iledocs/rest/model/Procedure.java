package iledocs.rest.model;

import java.util.List;

public class Procedure extends IledocsModel {

    private String name;
    private Boolean exported;
    private List<Parameter> parameters;
    private ReturnValue returnValue;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getExported() {
        return exported;
    }

    public void setExported(Boolean exported) {
        this.exported = exported;
    }

    public List<Parameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<Parameter> parameters) {
        this.parameters = parameters;
    }

    public ReturnValue getReturnValue() {
        return returnValue;
    }

    public void setReturnValue(ReturnValue returnValue) {
        this.returnValue = returnValue;
    }

    @Override
    public String toString() {
        return name;
    }
}
