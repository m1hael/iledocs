package iledocs.rest.model;

import java.util.LinkedList;
import java.util.List;

public class IledocsModel {

	public static enum Tag {
		Author("author"), 
		Date("date"), 
		Brief("brief"), 
		Link("link"), 
		Revision("rev"), 
		Deprecated("deprecated"), 
		Return("return"), 
		Parameter("param"), 
		Project("project"),
		Warning("warning"), 
		Info("info"), 
		EscapeMessage("throws"), 
		Interface("interface"), 
		Implements("implements"),
		Version("version");

		Tag(String keyword) {
			this.keyword = keyword;
		}

		private final String keyword;

		public String keyword() {
			return keyword;
		}

		@Override
		public String toString() {
			return keyword;
		}
	}

	private String project;
	private boolean flagInterface;
	private String implementation;
	private List<EscapeMessage> escapeMessages = new LinkedList<EscapeMessage>();
	private List<String> infos = new LinkedList<String>();
	private List<String> warnings = new LinkedList<String>();
	private String title;
	private String description;
	private String date;
	private String author;
	private List<Revision> revisions = new LinkedList<Revision>();
	private List<Link> links = new LinkedList<Link>();
	private String deprecated;
	private Variable returnValue;
	private List<Parameter> parameters = new LinkedList<Parameter>();
	private String version;

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public List<String> getInfos() {
		return infos;
	}

	public void setInfos(List<String> infos) {
		this.infos = infos;
	}

	public List<String> getWarnings() {
		return warnings;
	}

	public void setWarnings(List<String> warnings) {
		this.warnings = warnings;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public List<Revision> getRevisions() {
		return revisions;
	}

	public void setRevisions(List<Revision> revisions) {
		this.revisions = revisions;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	public String getDeprecated() {
		return deprecated;
	}

	public void setDeprecated(String deprecated) {
		this.deprecated = deprecated;
	}

	public Variable getReturnValue() {
		return returnValue;
	}

	public void setReturnValue(Variable returnValue) {
		this.returnValue = returnValue;
	}

	public List<Parameter> getParameters() {
		return parameters;
	}

	public void setParameters(List<Parameter> parameters) {
		this.parameters = parameters;
	}

	public boolean isFlagInterface() {
		return flagInterface;
	}

	public void setFlagInterface(boolean flagInterface) {
		this.flagInterface = flagInterface;
	}

	public String getImplementation() {
		return implementation;
	}

	public void setImplementation(String implementation) {
		this.implementation = implementation;
	}

	public List<EscapeMessage> getEscapeMessages() {
		return escapeMessages;
	}

	public void setEscapeMessages(List<EscapeMessage> escapeMessages) {
		this.escapeMessages = escapeMessages;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public String toString() {
		return title;
	}
}
