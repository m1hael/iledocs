package iledocs.rest;

import com.codahale.metrics.health.HealthCheck;
import com.mongodb.client.MongoClient;

/**
 * Basic checking of MongoDB connection.
 * 
 * @author Mihael Schmidt
 */
public class MongoHealthCheck extends HealthCheck {
    
    private final MongoClient mongo;

    public MongoHealthCheck(MongoClient mongo) {
        this.mongo = mongo;
    }
    
    @Override
    protected Result check() throws Exception {
        mongo.listDatabaseNames();
        return Result.healthy();
    }
    
}
