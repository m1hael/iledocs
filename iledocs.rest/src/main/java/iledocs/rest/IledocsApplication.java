package iledocs.rest;

import iledocs.rest.resource.IledocsResource;
import iledocs.rest.resource.IndexResource;
import iledocs.rest.resource.SearchResource;
import iledocs.rest.resource.UtilResource;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.web.WebBundle;
import io.dropwizard.web.conf.WebConfiguration;

import org.bson.Document;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;

public class IledocsApplication extends Application<IledocsConfiguration> {

    public static void main(String[] args) throws Exception {
        new IledocsApplication().run(args);
    }

    @Override
    public String getName() {
        return "iledocs";
    }

    @Override
    public void initialize(Bootstrap<IledocsConfiguration> bootstrap) {
        bootstrap.addBundle(new AssetsBundle("/web", "/web", "index.html", "iledocs.web"));
        bootstrap.addBundle(new WebBundle<IledocsConfiguration>() {
            @Override
            public WebConfiguration getWebConfiguration(final IledocsConfiguration configuration) {
                return configuration.getWebConfiguration();
            }
        });
    }

    @Override
    public void run(IledocsConfiguration configuration, Environment environment) throws Exception {
        final MongoClient mongo = MongoClients.create(configuration.mongourl);
        final MongoManaged mongoManaged = new MongoManaged(mongo);
        environment.lifecycle().manage(mongoManaged);
 
        environment.healthChecks().register("MongoDB Health Check", new MongoHealthCheck(mongo));
        
        environment.jersey().register(new UtilResource());
        
        MongoCollection<Document> collection = mongoManaged.getDatabase().getCollection("iledocs");
        
        environment.jersey().register(new SearchResource(collection));
        environment.jersey().register(new IledocsResource(mongo));
        environment.jersey().register(new IndexResource(collection));
    }

}
