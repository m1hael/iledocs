package iledocs.rest;

import org.apache.commons.lang3.StringUtils;

public class Version implements Comparable<Version> {

	public Integer major;
	public Integer minor;
	public Integer revision;
	public Integer build;
	public String qualifier;
	public final String string;

	public Version(String versionString) {
		this.string = versionString;
		parse(versionString);
	}

	private void parse(String versionString) {
		if (StringUtils.isNotBlank(versionString)) {

			StringBuilder s = new StringBuilder();

			for (int i = 0; i < versionString.length(); i++) {
				char c = versionString.charAt(i);
				if (c == '.') {
					if (s.length() > 0) {
						assignNextValue(Integer.parseInt(s.toString()));
						s.setLength(0);
					}
				} 
				else if (c == '-') {
					// rest is qualifier
					if (versionString.length() - 1 > i) {
						qualifier = versionString.substring(i + 1);
					}
					break;
				} 
				else {
					s.append(c);
				}
			}
			
			if (s.length() > 0) {
				assignNextValue(Integer.parseInt(s.toString()));
			}
		}

		major = major == null ? 0 : major;
		minor = minor == null ? 0 : minor;
		revision = revision == null ? 0 : revision;
		build = build == null ? 0 : build;
		qualifier = qualifier == null ? StringUtils.EMPTY : qualifier;
	}

	private final void assignNextValue(Integer value) {
		if (major == null) {
			major = value;
		} else if (minor == null) {
			minor = value;
		} else if (revision == null) {
			revision = value;
		} else if (build == null) {
			build = value;
		} else {
			throw new RuntimeException("Unsupported version number");
		}
	}
	
	@Override
	public int compareTo(Version v) {
		int returnValue = major.compareTo(v.major);
		if (returnValue == 0) {
			returnValue = minor.compareTo(v.minor);

			if (returnValue == 0) {
				returnValue = revision.compareTo(v.revision);

				if (returnValue == 0) {
					returnValue = build.compareTo(v.build);

					if (returnValue == 0) {
						if (qualifier.isEmpty() && v.qualifier.isEmpty()) {
							returnValue = 0;
						}
						else if (qualifier.isEmpty() && !v.qualifier.isEmpty()) {
							returnValue = 1;
						}
						else if (!qualifier.isEmpty() && v.qualifier.isEmpty()) {
							returnValue = -1;
						}
						else {
							returnValue = qualifier.compareTo(v.qualifier);
						}
					}
				}
			}
		}

		return returnValue;
	}

	@Override
	public String toString() {
		if (StringUtils.isEmpty(qualifier)) {
			return major + "." + minor + "." + revision + "." + build;
		} 
		else {
			return major + "." + minor + "." + revision + "." + build + "-"
					+ qualifier;
		}
	}
}