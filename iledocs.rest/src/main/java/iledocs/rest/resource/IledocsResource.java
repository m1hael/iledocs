package iledocs.rest.resource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.bson.UuidRepresentation;
import org.bson.types.ObjectId;
import org.mongojack.DBQuery;
import org.mongojack.DBQuery.Query;
import org.mongojack.JacksonMongoCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.databind.util.JSONWrappedObject;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.WriteConcern;
import com.mongodb.client.DistinctIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.result.UpdateResult;

import iledocs.rest.Indexer;
import iledocs.rest.Version;
import iledocs.rest.model.Program;
import iledocs.rest.model.Project;

@Path("/")
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public class IledocsResource {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final Indexer indexer = new Indexer();
	private final JacksonMongoCollection<Program> programDatabase;
	private final JacksonMongoCollection<Project> projectDatabase;
	private final MongoCollection<Document> rawDatabase;
    
    public IledocsResource(MongoClient mongoClient) {
        this.rawDatabase = mongoClient.getDatabase("iledocs").getCollection("iledocs");
		this.programDatabase = JacksonMongoCollection.builder().build(mongoClient, "iledocs", "iledocs", Program.class, UuidRepresentation.JAVA_LEGACY);
		this.projectDatabase = JacksonMongoCollection.builder().build(mongoClient, "iledocs", "iledocs", Project.class, UuidRepresentation.JAVA_LEGACY);
    }

    @GET
    @Timed
    public Object list(@QueryParam("callback") String jsonpCallbackPrefix) {
    	List<String> projectNames = new ArrayList<String>();
    	Consumer<String> push = name -> projectNames.add(name);
    	
        DistinctIterable<String> distinctValues = rawDatabase.distinct("project", String.class);
        distinctValues.forEach(push);
        
        if (jsonpCallbackPrefix == null || jsonpCallbackPrefix.isEmpty()) {
            return Response.ok(projectNames).build();
        }
        else {
            return new JSONWrappedObject(jsonpCallbackPrefix + "(", ")", projectNames);
        }
    }
    
    @GET
    @Path("{project}")
    @Timed
    public Object getProjectInfo(@PathParam("project") String project, @QueryParam("version") String version ,@QueryParam("callback") String jsonpCallbackPrefix) {
    	Query query;
    	if (version != null) {
    		query = DBQuery.is("clazz", "project").is("project", project).is("version", version);
    	}
    	else {
    		List<String> versions = getProjectInfoVersions(project);
	        if (versions.isEmpty()) {
	        	query = DBQuery.is("clazz", "project").is("project", project);
	        }
	        else if (versions.get(0).isEmpty()){
	        	query = DBQuery.is("clazz", "project").is("project", project);
	        }
	        else {
	        	query = DBQuery.is("clazz", "project").is("project", project).is("version", versions.get(0));
	        }
    	}
    	
    	Project projectInfo = projectDatabase.findOne(query);
        if (projectInfo == null) {
            logger.info("Project info not found in project {}", project);
            throw new WebApplicationException(Status.NOT_FOUND);
        }
        else {
        	
	    	if (jsonpCallbackPrefix == null || jsonpCallbackPrefix.isEmpty()) {
	            return Response.ok(projectInfo).build();
	        }
	        else {
	            return new JSONWrappedObject(jsonpCallbackPrefix + "(", ")", projectInfo);
	        }
	    	
        }
    }
    
    @GET
    @Path("{project}/versions")
    @Timed
    public Object listProjectInfoVersions(@PathParam("project") String project, @QueryParam("callback") String jsonpCallbackPrefix) {
    	List<String> versionStrings = getProjectInfoVersions(project);
    	
    	if (jsonpCallbackPrefix == null || jsonpCallbackPrefix.isEmpty()) {
            return Response.ok(versionStrings).build();
        }
        else {
            return new JSONWrappedObject(jsonpCallbackPrefix + "(", ")", versionStrings);
        }
    }
    
    @SuppressWarnings("deprecation")
	@GET
    @Path("{project}/modules")
    @Timed
    public Object listModules(@PathParam("project") String project, @QueryParam("callback") String jsonpCallbackPrefix) {
        if (StringUtils.isEmpty(project)) {
            throw new WebApplicationException(Status.BAD_REQUEST);
        }
        
        Set<String> programs = new HashSet<String>();
        
        MongoCursor<Program> cursor = programDatabase.find(DBQuery.is("clazz", "program").is("project", project)).cursor();
        while (cursor.hasNext()) {
            programs.add(cursor.next().getSource());
        }
        
        if (jsonpCallbackPrefix == null || jsonpCallbackPrefix.isEmpty()) {
            return Response.ok(programs).build();
        }
        else {
            return new JSONWrappedObject(jsonpCallbackPrefix + "(", ")", programs);
        }
    }

    @GET
    @Path("{project}/modules/{program}/versions")
    @Timed
    public Object listProgramVersions(@PathParam("project") String project, @PathParam("program") String program, @QueryParam("callback") String jsonpCallbackPrefix) {
    	if (StringUtils.isEmpty(project) || StringUtils.isEmpty(program)) {
            throw new WebApplicationException(Status.BAD_REQUEST);
        }
    	
    	List<String> versionStrings = getProgramVersions(project, program);
    	
    	if (jsonpCallbackPrefix == null || jsonpCallbackPrefix.isEmpty()) {
            return Response.ok(versionStrings).build();
        }
        else {
            return new JSONWrappedObject(jsonpCallbackPrefix + "(", ")", versionStrings);
        }
    }

	private List<String> getProgramVersions(String project, String program) {
		List<Version> versions = new ArrayList<>();
    	List<String> versionStrings = new ArrayList<String>();
    	
    	Query query = DBQuery.is("clazz", "program").is("project", project).is("source", program);
    	MongoCursor<Program> cursor = programDatabase.find(query).cursor();
        while (cursor.hasNext()) {
        	Program iledocsProgram = cursor.next();
        	versions.add(new Version(StringUtils.defaultString(iledocsProgram.getVersion())));
        }
        
        Collections.sort(versions);
        for (int i = versions.size()-1; i >= 0; i--) {
        	versionStrings.add(versions.get(i).string);
        }
		return versionStrings;
	}
	
	private List<String> getProjectInfoVersions(String project) {
		List<Version> versions = new ArrayList<>();
    	List<String> versionStrings = new ArrayList<String>();
    	
    	Query query = DBQuery.is("clazz", "project").is("project", project);
    	MongoCursor<Project> cursor = projectDatabase.find(query).cursor();
        while (cursor.hasNext()) {
        	Project p = cursor.next();
        	versions.add(new Version(StringUtils.defaultString(p.getVersion())));
        }
        
        Collections.sort(versions);
        for (int i = versions.size()-1; i >= 0; i--) {
        	versionStrings.add(versions.get(i).string);
        }
		return versionStrings;
	}
    
    @GET
    @Path("{project}/modules/{program}")
    @Timed
    public Object get(@PathParam("project") String project, @PathParam("program") String program, @QueryParam("version") String version, @QueryParam("callback") String jsonpCallbackPrefix) {
        if (StringUtils.isEmpty(project)) {
            throw new WebApplicationException(Status.BAD_REQUEST);
        }
        
        Query query;
        
        if (version != null && StringUtils.isEmpty(version)) {
        	query = DBQuery.is("clazz", "program").is("project", project).is("source", program).notExists("version");
        }
        else if (version != null) {
        	query = DBQuery.is("clazz", "program").is("project", project).is("source", program).is("version", version);
        }
        else {
	        List<String> versions = getProgramVersions(project, program);
	        if (versions.isEmpty()) {
	        	query = DBQuery.is("clazz", "program").is("project", project).is("source", program);
	        }
	        else if (versions.get(0).isEmpty()){
	        	query = DBQuery.is("clazz", "program").is("project", project).is("source", program);
	        }
	        else {
	        	query = DBQuery.is("clazz", "program").is("project", project).is("source", program).is("version", versions.get(0));
	        }
        }
        
        Program p = programDatabase.findOne(query);
        if (p == null) {
            logger.info("Program {} not found in project {}", program, project);
            throw new WebApplicationException(Status.NOT_FOUND);
        }
        else {
            
            if (jsonpCallbackPrefix == null || jsonpCallbackPrefix.isEmpty()) {
                return Response.ok(p).build();
            }
            else {
                return new JSONWrappedObject(jsonpCallbackPrefix + "(", ")", p);
            }
            
        }
    }
    
    @POST
    @Path("{project}/modules/{program}")
    @Timed
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public void persist(@PathParam("project") String project, @PathParam("program") String program, Program data) {
        if (data == null) {
            throw new WebApplicationException(Status.BAD_REQUEST);
        }
        
        logger.info("Adding program {} to project {}", program, project);
        
        if (StringUtils.isEmpty(data.getProject())) {
            data.setProject(project);
        }
        
        String documentId = null;
        Query query;
        if (StringUtils.isNotBlank(data.getVersion())) {
        	query = DBQuery.is("clazz", "program").is("project", data.getProject()).is("source", program).is("version", data.getVersion());
        }
        else {
        	query = DBQuery.is("clazz", "program").is("project", data.getProject()).is("source", program).notExists("version");
        }
        Program persistedProgram = programDatabase.findOne(query);
        if (persistedProgram == null) {
        	UpdateResult result = programDatabase.save(data, WriteConcern.ACKNOWLEDGED);
            logger.info("Program {} inserted", program);
            documentId = programDatabase.findOne(query).getId();
        }
        else {
        	persistedProgram = programDatabase.findOneAndReplace(query, data);
            logger.info("Program {} updated", program);
            
            documentId = persistedProgram.getId();
        }
        
        try {
        	logger.debug("Persisted document id " + documentId);
        	BasicDBObject filterById = new BasicDBObject("_id", new ObjectId(documentId));
        	Document document = rawDatabase.find(filterById).first();
        	if (document.containsKey("index")) {
        		document.remove("index");
        	}
        	
        	BasicDBList index = indexer.buildIndex(document);
        	document.put("index", index);
        	rawDatabase.findOneAndReplace(filterById, document);
        	logger.info("Program {} updated with index data", program);
        }
        catch(Exception e) {
        	logger.error("No id returned from write/update action. Cannot insert index data. Program: " + program);
        }
    }
    
    @Path("{project}")
    @POST
    @Timed
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public void persist(@PathParam("project") String project, Project projectInfo) {
        if (projectInfo == null) {
            throw new WebApplicationException(Status.BAD_REQUEST);
        }
        
        logger.info("Adding project info to project {}", project);
        
        Query query;
        if (StringUtils.isNotBlank(projectInfo.getVersion())) {
        	query = DBQuery.is("clazz", "project").is("project", project).is("version", projectInfo.getVersion());
        }
        else {
        	query = DBQuery.is("clazz", "project").is("project", project).notExists("version");
        }
        
        Project persistedProject = projectDatabase.findOne(query);
        if (persistedProject == null) {
        	projectDatabase.insert(projectInfo);
            logger.info("Project info for {} inserted", project);
        }
        else {
        	projectDatabase.findOneAndReplace(query, projectInfo);
            logger.info("Project info update for {}", project);
        }
    }
    
    @DELETE
    @Timed
    @Path("{project}/modules/{program}")
    public void deleteProgram(@PathParam("project") String project, @PathParam("program") String program) {
        if (project == null || program == null) {
            throw new WebApplicationException(Status.BAD_REQUEST);
        }
        
        logger.info("Deleting program {} from project {}", program, project);
        
        programDatabase.findOneAndDelete(DBQuery.is("clazz", "program").is("project", project).is("source", program));
        
        logger.info("Program {} deleted", program);
    }
    
    @DELETE
    @Timed
    @Path("{project}")
    public void deleteProject(@PathParam("project") String project, @QueryParam("scope") String scope) {
        if (project == null) {
            throw new WebApplicationException(Status.BAD_REQUEST);
        }
        
        logger.info("Deleting project {}", project);
        
        if ("full".equals(scope)) {
        	programDatabase.findOneAndDelete(DBQuery.is("clazz", "program").is("project", project));
        }
        
        projectDatabase.findOneAndDelete(DBQuery.is("clazz", "project").is("project", project));
        
        logger.info("Project {} deleted", project);
    }
}

