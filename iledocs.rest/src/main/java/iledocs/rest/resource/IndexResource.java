package iledocs.rest.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.bson.Document;

import com.codahale.metrics.annotation.Timed;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

import iledocs.rest.Indexer;

@Path("/")
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public class IndexResource {

	private final MongoCollection<Document> rawDatabase;
	private final Indexer indexer = new Indexer();
	
	public IndexResource(MongoCollection<Document> rawDatabase) {
		this.rawDatabase = rawDatabase;
	}
	
	@GET
    @Path("/index")
    @Timed
    public void index() {
		
		MongoCursor<Document> cursor = rawDatabase.find(new BasicDBObject("clazz", "program")).cursor();
		while (cursor.hasNext()) {
			Document document = cursor.next();
			BasicDBObject filter = new BasicDBObject("_id", document.getObjectId("_id"));
			updateIndex(document);
			rawDatabase.replaceOne(filter, document);
		}
	}

	private void updateIndex(Document document) {
		document.remove("index");
		
		BasicDBList indexData = indexer.buildIndex(document);
		
		document.put("index", indexData);
	}
}
