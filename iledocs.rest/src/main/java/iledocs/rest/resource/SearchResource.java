package iledocs.rest.resource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import org.bson.BSONObject;
import org.bson.Document;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.databind.util.JSONWrappedObject;
import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

import iledocs.rest.model.SearchResult;
import iledocs.rest.model.SearchResultEntry;

@Path("/")
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public class SearchResource {

	private final MongoCollection<Document> rawDatabase;
    private final List<SearchResultEntry.Type> types = new ArrayList<>();
    
    public SearchResource(MongoCollection<Document> collection) {
        this.rawDatabase = collection;
        
        types.add(SearchResultEntry.Type.Module);
        types.add(SearchResultEntry.Type.Procedure);
        types.add(SearchResultEntry.Type.Variable);
        types.add(SearchResultEntry.Type.File);
        types.add(SearchResultEntry.Type.Constant);
    }
    
    @GET
    @Path("/search")
    @Timed
    public Object search(@QueryParam("q") String searchTerm, @QueryParam("callback") String jsonpCallbackPrefix) {
    	if (searchTerm == null || searchTerm.isEmpty()) {
    		throw new WebApplicationException(Status.BAD_REQUEST);
    	}
    	
    	int count = 0;
    	
    	SearchResult result = new SearchResult();
    	result.setSearchTerm(searchTerm);
    	
    	searchTerm = searchTerm.toLowerCase();
    	
    	Document regexQuery = new Document();
    	regexQuery.append("$regex", ".*" + Pattern.quote(searchTerm) + ".*");
    	BasicDBObject criteria = new BasicDBObject("index.term", regexQuery);
    	MongoCursor<Document> cursor = rawDatabase.find(criteria).cursor();
    	
    	while (cursor.hasNext()) {
    		Document object = cursor.next();
    		List<SearchResultEntry> entries = createSearchResultEntries(searchTerm, object);
    		result.addEntries(entries);
    		count += entries.size();
    	}
    	
    	result.setHits(count);
    	
    	sortEntries(result.getEntries());
    	
       	if (jsonpCallbackPrefix == null) {
    		return result;
    	}
    	else {
    		return new JSONWrappedObject(jsonpCallbackPrefix + "(", ")", result);
    	}
    }

	private List<SearchResultEntry> createSearchResultEntries(String searchTerm, Document object) {
		List<SearchResultEntry> entries = new ArrayList<>();
		
		List<Document> indexed = (List<Document>) object.get("index");
		
		for (Document doc : indexed) {
			String value = doc.getString("term");
//			
			if (value.contains(searchTerm)) {
				entries.add(new SearchResultEntry(
						object.getString("project"),
						object.getString("source"), 
						doc.getString("originalValue"),
						SearchResultEntry.Type.valueOf(doc.getString("type"))));
			}
		}
		
		return entries;
	}
	
	private void sortEntries(List<SearchResultEntry> entries) {
		Collections.sort(entries, new Comparator<SearchResultEntry>() {
			@Override
			public int compare(SearchResultEntry e1, SearchResultEntry e2) {
				if (e1.getType() == e2.getType()) {
					return e1.getValue().compareTo(e2.getValue());
				}
				else {
					return types.indexOf(e1.getType()) - types.indexOf(e2.getType());
				}
			}
		});
	}
}
