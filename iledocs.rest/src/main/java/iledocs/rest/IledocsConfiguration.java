package iledocs.rest;

import io.dropwizard.Configuration;
import io.dropwizard.web.conf.WebConfiguration;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IledocsConfiguration extends Configuration {

	@Valid
    @NotNull
    @JsonProperty("web")
    private WebConfiguration webConfiguration = new WebConfiguration();

    public WebConfiguration getWebConfiguration() {
        return webConfiguration;
    }

    public void setWebConfiguration(final WebConfiguration webConfiguration) {
        this.webConfiguration = webConfiguration;
    }
    
    @JsonProperty
    @NotNull
    public String mongourl = "mongodb://localhost/iledocs";
	
}
