package iledocs.rest;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.bson.Document;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;

import iledocs.rest.model.SearchResultEntry;

public class Indexer {

	public BasicDBList buildIndex(Document document) {
		BasicDBList index = new BasicDBList();
		index.add(createIndexTerm(getString(document, "source"), SearchResultEntry.Type.Module.name()));
		
		if (StringUtils.isNotBlank(getString(document, "title"))) {
			index.add(createIndexTerm(getString(document, "title"), SearchResultEntry.Type.Module.name()));
		}
		
		// procedures
		List procedures = (List) document.get("procedures");
		if (procedures != null) {
			Iterator<Object> iterator = procedures.iterator();
			while (iterator.hasNext()) {
				Document procedure = (Document) iterator.next();
				index.add(createIndexTerm(procedure.getString("name"), SearchResultEntry.Type.Procedure.name()));
				
				if (StringUtils.isNotBlank(procedure.getString("title"))) {
					index.add(createIndexTerm(procedure.getString("title"), SearchResultEntry.Type.Procedure.name()));
				}
			}
		}
		
		// variables
		List variables = (List) document.get("variables");
		if (variables != null) {
			Iterator<Object> iterator = variables.iterator();
			while (iterator.hasNext()) {
				Document variable = (Document) iterator.next();
				index.add(createIndexTerm(variable.getString("name"), SearchResultEntry.Type.Variable.name()));
			}
		}
		
		// constants
		List constants = (List) document.get("constants");
		if (constants != null) {
			Iterator<Object> iterator = constants.iterator();
			while (iterator.hasNext()) {
				Document constant = (Document) iterator.next();
				index.add(createIndexTerm(constant.getString("name"), SearchResultEntry.Type.Constant.name()));
			}
		}
		
		// files
		List files = (List) document.get("files");
		if (files != null) {
			Iterator<Object> iterator = files.iterator();
			while (iterator.hasNext()) {
				Document file = (Document) iterator.next();
				index.add(createIndexTerm(file.getString("name"), SearchResultEntry.Type.File.name()));
			}
		}
		
		return index;
	}
	
	private Object createIndexTerm(String term, String type) {
		BasicDBObject indexedTerm = new BasicDBObject();
		indexedTerm.put("term", term.toLowerCase());
		indexedTerm.put("originalValue", term);
		indexedTerm.put("type", type);
		return indexedTerm;
	}
	
	private String getString(Document document, String key) {
		if (document.containsKey(key)) {
			Object value = document.get(key);
			return value == null ? null : value.toString();
		}
		else {
			return null;
		}
	}
}
