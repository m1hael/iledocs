'use strict';

angular.module("iledocs.web.services")
        .factory("projectService", function ($http, config, toastr) {
            
            var url = config.rest.iledocs;
    
            return {
                
                list: function (callback) {
                    var httpConfig = {
                            "method": "GET",
                            "headers": {
                                "Accept": "application/json"
                            }
                        };
                        
                    $http.jsonp(url, httpConfig).then(
                            function success(response) {
                                callback.success(response.data);
                            },
                            function error(response) {
                                callback.error(response.status, response.statusText);
                            });
                },
                get: function(project, callback) {
                    var httpConfig = {
                            "method": "GET",
                            "headers": {
                                "Accept": "application/json"
                            }
                        };
                        
                    $http.jsonp(url + "/" + project, httpConfig).then(
                            function success(response) {
                                callback.success(response.data);
                            },
                            function error(response) {
                                if (response.status === 404) {
                                    callback.success(null);
                                }
                                else {
                                    callback.error(response.status, response.statusText);
                                }
                            });
                }
            };
        });
        