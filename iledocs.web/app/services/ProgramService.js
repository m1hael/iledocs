'use strict';

angular.module("iledocs.web.services")
        .factory("programService", function ($http, config) {
            
            var url = config.rest.iledocs;
    
            return {
                
                list: function (project, callback) {
                    var httpConfig = {
                            "method": "GET",
                            "headers": {
                                "Accept": "application/json"
                            }
                        };
                    
                    $http.jsonp(url + "/" + project + "/modules", httpConfig).then(
                            function success(response) {
                                callback.success(project, response.data);
                            },
                            function error(response) {
                                callback.error(response.status, response.statusText);
                            });
                },
                
                get: function (project, program, version, callback) {
                    var httpConfig = {
                            "method": "GET",
                            "headers": {
                                "Accept": "application/json"
                            }
                        };
                        
                    var restUrl = url + "/" + project + "/modules/" + program;
                    if (version && version == "no version") {
                        restUrl = restUrl + "?version=";
                    }
                    else if (version) {
                        restUrl = restUrl + "?version=" + version;
                    }
            
                    $http.jsonp(restUrl, httpConfig).then(
                            function success(response) {
                                var module = processModule(response.data);
                                callback.success(module);
                            },
                            function error(response) {
                                callback.error(response.status, response.statusText);
                            });
                },
                
                versions: function (project, program, callback) {
                    var httpConfig = {
                            "method": "GET",
                            "headers": {
                                "Accept": "application/json"
                            }
                        };
                        
                    $http.jsonp(url + "/" + project + "/modules/" + program + "/versions", httpConfig).then(
                            function success(response) {
                                var module = processModule(response.data);
                                callback.success(module);
                            },
                            function error(response) {
                                callback.error(response.status, response.statusText);
                            });
                }
            };
    
            function processModule(module) {
                // create a short version of the module description
                if (module.description && module.description.length >= 100) {
                    // TODO not really safe because it may mess up html code embedded in the description
                    module.shortDescription = module.description.substring(0, 100) + "...";
                }
                else {
                    module.shortDescription = module.description;
                }
                
                return module;
            }
        });
        