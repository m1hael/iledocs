'use strict';

angular.module("iledocs.web.services")
    .factory("searchService", function ($http, config) {

    return {

        search: function (searchText, callback) {
            var httpConfig = {
                "method": "GET",
                "headers": {
                    "Accept": "application/json"
                }
            };

            var url = config.rest.iledocs + "/search?q=" + searchText;
            
            $http.jsonp(url, httpConfig).then(
                function success(response) {
                    callback.success(response.data);
                },
                function error(response) {
                    callback.error(response.status, response.statusText);
                });
        }
    }
});