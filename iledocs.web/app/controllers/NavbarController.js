'use strict';

/* Controllers */

angular.module("iledocs.web.controllers")
    .controller("NavbarController", function ($rootScope, $location, searchService, toastr) {

    document.getElementById("searchInputField").select();
    
    var searchCallback = {
        success: function (searchResult) {
            $rootScope.searchResult = undefined;
            
            if (searchResult && searchResult.hits && searchResult.hits > 0) {
                if (searchResult.hits === 1) {
                    $location.path(searchResult.entries[0].project + "/" + searchResult.entries[0].source);
                }
                else {
                    $rootScope.searchResult = searchResult;
                    $('#searchResultDialog').modal();
                }
            }
            else {
                toastr.info("No search entries for " + searchResult.searchTerm + ".");
            }
        },
        error: function (status, statusText) {
            $rootScope.searchResult = undefined;
            
            if (status === 404) {
                toastr.error("The search REST service is not available.");
            }
            else {
                toastr.error("The search request failed.<br/>" +
                             status + " - " + statusText, undefined, {allowHtml: true});
            }
        }
    };

    this.search = function(searchText) {
        if (searchText == undefined || searchText.trim() == "") {
            // nix
            document.getElementById("searchInputField").focus();
        }
        else {
            searchService.search(searchText, searchCallback);
        }
    };

});