'use strict';

/* Controllers */

angular.module("iledocs.web.controllers")
    .controller("IledocsController", function ($scope, $rootScope, projectService, programService, toastr) {

    var projectListCallback = {
        success: function (data) {
            $scope.projects = undefined;

            if (data) {
                $scope.projects = data;
            }
            else {
                toastr.info("No projects available");
            }
        },
        error: function (status, statusText) {
            $scope.projects = undefined;

            if (status === 404) {
                toastr.error("The project REST service is not available.");
            }
            else {
                toastr.error("The search request failed.<br/>" +
                             status + " - " + statusText, undefined, {allowHtml: true});
            }
        }
    };
    
    $scope.scrollTo = function(eID) {
        var element_to_scroll_to = document.getElementById(eID);
        element_to_scroll_to.scrollIntoView();
    };
    
    // loads the projects to initialize the list
    // the callback function adds the list to the variables in the scope
    projectService.list(projectListCallback);

});
