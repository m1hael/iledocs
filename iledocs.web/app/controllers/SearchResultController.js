'use strict';

/* Controllers */

angular.module("iledocs.web.controllers")
    .controller('SearchResultController', function($scope, $rootScope, toastr, searchService, $location) {

    var searchCallback = {
        success: function (searchResult) {
            $rootScope.searchResult = undefined;

            if (searchResult && searchResult.hits && searchResult.hits > 0) {
                $rootScope.searchResult = searchResult;
            }
            else {
                toastr.info("No search entries for " + searchResult.searchTerm + ".");
            }

            document.getElementById("searchResultDialogInputField").select();
        },
        error: function (status, statusText) {
            $rootScope.searchResult = undefined;

            if (status === 404) {
                toastr.error("The search REST service is not available.");
            }
            else {
                toastr.error("The search request failed.<br/>" +
                             status + " - " + statusText, undefined, {allowHtml: true});
            }

            document.getElementById("searchResultDialogInputField").select();
        }
    };

    this.search = function(searchText) {
        if (searchText == undefined || searchText.trim() == "") {
            // nix
            selectInputBox();
        }
        else {
            searchService.search(searchText, searchCallback);
        }        
    };

    this.select = function(project, module) {
        $('#searchResultDialog').modal("hide");
        $location.path(project + "/" + module);
    }
});