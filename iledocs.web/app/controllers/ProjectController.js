'use strict';

/* Controllers */

angular.module("iledocs.web.controllers")
    .controller("ProjectController", function ($scope, $routeParams, projectService, programService, toastr) {

    var programListCallback = {
        success: function (project, data) {
            $scope.programs = undefined;

            if (data) {
                $scope.project = project;
                $scope.programs = data;
            }
            else {
                toastr.info("No programs available for " + project + ".");
            }
        },
        error: function (status, statusText) {
            $scope.programs = undefined;
            if (status === 404) {
                toastr.error("The program REST service is not available.");
            }
            else {
                toastr.error("The search request failed.<br/>" +
                             status + " - " + statusText, undefined, {allowHtml: true});
            }
        }
    };

    var projectInfoCallback = {
        success: function (data) {
            if (data) {
                $scope.projectinfo = data;
            }
            else {
                $scope.projectinfo = undefined;
            }
        },
        error: function (status, statusText) {
            $scope.projectinfo = undefined;
        }
    };

    programService.list($routeParams.project, programListCallback);
    projectService.get($routeParams.project, projectInfoCallback);
});