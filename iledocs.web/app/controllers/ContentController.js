'use strict';

/* Controllers */

angular.module("iledocs.web.controllers")
    .controller("ContentController", function ($scope, $routeParams, projectService, programService, toastr) {

    var programListCallback = {
        success: function (project, data) {
            $scope.programs = undefined;

            if (data) {
                $scope.project = project;
                $scope.programs = data;
            }
            else {
                toastr.info("No programs available for " + project + ".");
            }
        },
        error: function (status, statusText) {
            $scope.programs = undefined;
            if (status === 404) {
                toastr.error("The program REST service is not available.");
            }
            else {
                toastr.error("The search request failed.<br/>" +
                             status + " - " + statusText, undefined, {allowHtml: true});
            }
        }
    };
    
    var programVersionCallback = {
        success: function (data) {
            $scope.versions = undefined;
            
            if (data.length > 0) {
                for (var i = data.length-1; i >= 0; i--) {
                    if (data[i].length == 0) {
                        data[i] = "no version";
                    }
                }
                
                $scope.versions = data; 
            }
        },
        error: function (status, statusText) {
            $scope.versions = undefined;
            if (status === 404) {
                toastr.error("The program versions REST service is not available.");
            }
            else {
                toastr.error("The versions request failed.<br/>" +
                             status + " - " + statusText, undefined, {allowHtml: true});
            }
        }
    };

    var programCallback = {
        success: function (data) {
            if (data) {
                $scope.program = data;
            }
            else {
                $scope.program = undefined;
                toastr.info("No documentation available.");
            }
        },
        error: function (status, statusText) {
            $scope.program = undefined;
            if (status === 404) {
                toastr.error("The program REST service is not available.");
            }
            else {
                toastr.error("The search request failed.<br/>" +
                             status + " - " + statusText, undefined, {allowHtml: true});
            }
        }
    };

    programService.list($routeParams.project, programListCallback);
    programService.get($routeParams.project, $routeParams.program, $routeParams.version, programCallback);
    programService.versions($routeParams.project, $routeParams.program, programVersionCallback);
});