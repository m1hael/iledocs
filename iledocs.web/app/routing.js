'use strict';

var app = angular.module('iledocs.web');

app.config(function ($routeProvider, $locationProvider) {
    $routeProvider
        .when("/:project/:program/:version", {
            templateUrl : "components/content-view.html",
            controller : "ContentController"
        })
        .when("/:project/:program", {
            templateUrl : "components/content-view.html",
            controller : "ContentController"
        })
        .when("/:project", {
            templateUrl : "components/project-view.html",
            controller : "ProjectController"
        })
        .when("/", {
            templateUrl : "components/initial-view.html"
        })
        .otherwise({redirectTo: '/'});

    $locationProvider.html5Mode(true);
});