'use strict';

var app = angular.module('iledocs.web');

app.constant("config", {
    "version" : "1.0.0",
    "rest" : {
        "iledocs" : "http://localhost:8080"
    }
})
.config(function($sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist(['**']);
});
