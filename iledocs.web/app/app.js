'use strict';

angular.module("iledocs.web.controllers", []);
angular.module("iledocs.web.services", []);

angular.module("iledocs.web", [
    "ngRoute",
    "ngAnimate",
    "toastr",
    "pasvaz.bindonce",
    "iledocs.web.controllers",
    "iledocs.web.services"
]);

angular.module("iledocs.web").filter("unsafe", function ($sce) {
    return function (val) {
        return $sce.trustAsHtml(val);
    };
});

