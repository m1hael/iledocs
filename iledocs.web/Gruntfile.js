module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        clean: ["dist", '.tmp'],        
        copy: {
            main: {
                files: [{
                    expand: true,
                    cwd: 'app/',
                    src: ['*.html', "img/*", '!js/**', '!lib/**', '!**/*.css', 'configuration.js'],
                    dest: 'dist/'
                },
                {
                    expand: true,
                    src: "app/components/*",
                    dest: "dist/components/",
                    flatten: true
                }]
            },
            bootstrap: {
                files: [{
                    expand: true,
                    src: "app/bower_components/bootstrap/dist/fonts/*",
                    dest: "dist/fonts/",
                    flatten: true
                }]
            }
        },
        useminPrepare: {
            html: 'app/index.html'
        },
        usemin: {
            html: ['dist/index.html']
        },
        uglify: {
            options: {
                report: 'min',
                mangle: false
            }
        },
        bower: {
            install: {
                //just run 'grunt bower:install' and you'll see files from your Bower packages in lib directory
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-bower-task');
    grunt.loadNpmTasks('grunt-usemin');

    // Tell Grunt what to do when we type "grunt" into the terminal
    grunt.registerTask('default', [
        'copy', 'useminPrepare', 'concat', 'uglify', 'cssmin', 'usemin'
    ]);
};