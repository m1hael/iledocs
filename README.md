# ILEDocs #

ILEDocs is a documentation tool which helps software developers to document their programs 
in a convenient way.

It is strongly inspired by the Java documentation tool Javadoc.

The goal of this project is to provide both a tool to parse the source code and extract the 
documentation information and also a platform to store and view the uploaded documentation.


## Backend ##

The backend is implemented using [Dropwizard](http://dropwizard.io) for easy REST service
creation. By using a REST service as an interface to the backend we don't limit this project 
to a special frontend or parser but can add documentation with various tools.


## Parser ##

### ILEDocs CL and RPG Parser ###

The old ILEDocs project has parsers for CL and RPG. The missing part is the upload to the new platform.
This could be achieved with the HTTP Client service program.

### MiWorkplace ###

MiWorkplace has parsers for CL and RPG and is able to update the documentation from within the IDE.


## Contribution guidelines ##

You can contribute in any way you like. Just do it! =)

Take a look at the wiki to get into the project.


## Who do I talk to? ##

Mihael Schmidt , mihael@rpgnextgen.com
